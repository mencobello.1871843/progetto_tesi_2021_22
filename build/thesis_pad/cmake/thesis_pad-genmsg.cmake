# generated from genmsg/cmake/pkg-genmsg.cmake.em

message(STATUS "thesis_pad: 2 messages, 1 services")

set(MSG_I_FLAGS "-Ithesis_pad:/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg;-Istd_msgs:/opt/ros/melodic/share/std_msgs/cmake/../msg")

# Find all generators
find_package(gencpp REQUIRED)
find_package(geneus REQUIRED)
find_package(genlisp REQUIRED)
find_package(gennodejs REQUIRED)
find_package(genpy REQUIRED)

add_custom_target(thesis_pad_generate_messages ALL)

# verify that message/service dependencies have not changed since configure



get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/LoginName.msg" NAME_WE)
add_custom_target(_thesis_pad_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "thesis_pad" "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/LoginName.msg" ""
)

get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/srv/RoomAddressee.srv" NAME_WE)
add_custom_target(_thesis_pad_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "thesis_pad" "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/srv/RoomAddressee.srv" ""
)

get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/Comunication.msg" NAME_WE)
add_custom_target(_thesis_pad_generate_messages_check_deps_${_filename}
  COMMAND ${CATKIN_ENV} ${PYTHON_EXECUTABLE} ${GENMSG_CHECK_DEPS_SCRIPT} "thesis_pad" "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/Comunication.msg" ""
)

#
#  langs = gencpp;geneus;genlisp;gennodejs;genpy
#

### Section generating for lang: gencpp
### Generating Messages
_generate_msg_cpp(thesis_pad
  "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/LoginName.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/thesis_pad
)
_generate_msg_cpp(thesis_pad
  "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/Comunication.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/thesis_pad
)

### Generating Services
_generate_srv_cpp(thesis_pad
  "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/srv/RoomAddressee.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/thesis_pad
)

### Generating Module File
_generate_module_cpp(thesis_pad
  ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/thesis_pad
  "${ALL_GEN_OUTPUT_FILES_cpp}"
)

add_custom_target(thesis_pad_generate_messages_cpp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_cpp}
)
add_dependencies(thesis_pad_generate_messages thesis_pad_generate_messages_cpp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/LoginName.msg" NAME_WE)
add_dependencies(thesis_pad_generate_messages_cpp _thesis_pad_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/srv/RoomAddressee.srv" NAME_WE)
add_dependencies(thesis_pad_generate_messages_cpp _thesis_pad_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/Comunication.msg" NAME_WE)
add_dependencies(thesis_pad_generate_messages_cpp _thesis_pad_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(thesis_pad_gencpp)
add_dependencies(thesis_pad_gencpp thesis_pad_generate_messages_cpp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS thesis_pad_generate_messages_cpp)

### Section generating for lang: geneus
### Generating Messages
_generate_msg_eus(thesis_pad
  "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/LoginName.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/thesis_pad
)
_generate_msg_eus(thesis_pad
  "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/Comunication.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/thesis_pad
)

### Generating Services
_generate_srv_eus(thesis_pad
  "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/srv/RoomAddressee.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/thesis_pad
)

### Generating Module File
_generate_module_eus(thesis_pad
  ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/thesis_pad
  "${ALL_GEN_OUTPUT_FILES_eus}"
)

add_custom_target(thesis_pad_generate_messages_eus
  DEPENDS ${ALL_GEN_OUTPUT_FILES_eus}
)
add_dependencies(thesis_pad_generate_messages thesis_pad_generate_messages_eus)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/LoginName.msg" NAME_WE)
add_dependencies(thesis_pad_generate_messages_eus _thesis_pad_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/srv/RoomAddressee.srv" NAME_WE)
add_dependencies(thesis_pad_generate_messages_eus _thesis_pad_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/Comunication.msg" NAME_WE)
add_dependencies(thesis_pad_generate_messages_eus _thesis_pad_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(thesis_pad_geneus)
add_dependencies(thesis_pad_geneus thesis_pad_generate_messages_eus)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS thesis_pad_generate_messages_eus)

### Section generating for lang: genlisp
### Generating Messages
_generate_msg_lisp(thesis_pad
  "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/LoginName.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/thesis_pad
)
_generate_msg_lisp(thesis_pad
  "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/Comunication.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/thesis_pad
)

### Generating Services
_generate_srv_lisp(thesis_pad
  "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/srv/RoomAddressee.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/thesis_pad
)

### Generating Module File
_generate_module_lisp(thesis_pad
  ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/thesis_pad
  "${ALL_GEN_OUTPUT_FILES_lisp}"
)

add_custom_target(thesis_pad_generate_messages_lisp
  DEPENDS ${ALL_GEN_OUTPUT_FILES_lisp}
)
add_dependencies(thesis_pad_generate_messages thesis_pad_generate_messages_lisp)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/LoginName.msg" NAME_WE)
add_dependencies(thesis_pad_generate_messages_lisp _thesis_pad_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/srv/RoomAddressee.srv" NAME_WE)
add_dependencies(thesis_pad_generate_messages_lisp _thesis_pad_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/Comunication.msg" NAME_WE)
add_dependencies(thesis_pad_generate_messages_lisp _thesis_pad_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(thesis_pad_genlisp)
add_dependencies(thesis_pad_genlisp thesis_pad_generate_messages_lisp)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS thesis_pad_generate_messages_lisp)

### Section generating for lang: gennodejs
### Generating Messages
_generate_msg_nodejs(thesis_pad
  "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/LoginName.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/thesis_pad
)
_generate_msg_nodejs(thesis_pad
  "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/Comunication.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/thesis_pad
)

### Generating Services
_generate_srv_nodejs(thesis_pad
  "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/srv/RoomAddressee.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/thesis_pad
)

### Generating Module File
_generate_module_nodejs(thesis_pad
  ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/thesis_pad
  "${ALL_GEN_OUTPUT_FILES_nodejs}"
)

add_custom_target(thesis_pad_generate_messages_nodejs
  DEPENDS ${ALL_GEN_OUTPUT_FILES_nodejs}
)
add_dependencies(thesis_pad_generate_messages thesis_pad_generate_messages_nodejs)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/LoginName.msg" NAME_WE)
add_dependencies(thesis_pad_generate_messages_nodejs _thesis_pad_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/srv/RoomAddressee.srv" NAME_WE)
add_dependencies(thesis_pad_generate_messages_nodejs _thesis_pad_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/Comunication.msg" NAME_WE)
add_dependencies(thesis_pad_generate_messages_nodejs _thesis_pad_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(thesis_pad_gennodejs)
add_dependencies(thesis_pad_gennodejs thesis_pad_generate_messages_nodejs)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS thesis_pad_generate_messages_nodejs)

### Section generating for lang: genpy
### Generating Messages
_generate_msg_py(thesis_pad
  "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/LoginName.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/thesis_pad
)
_generate_msg_py(thesis_pad
  "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/Comunication.msg"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/thesis_pad
)

### Generating Services
_generate_srv_py(thesis_pad
  "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/srv/RoomAddressee.srv"
  "${MSG_I_FLAGS}"
  ""
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/thesis_pad
)

### Generating Module File
_generate_module_py(thesis_pad
  ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/thesis_pad
  "${ALL_GEN_OUTPUT_FILES_py}"
)

add_custom_target(thesis_pad_generate_messages_py
  DEPENDS ${ALL_GEN_OUTPUT_FILES_py}
)
add_dependencies(thesis_pad_generate_messages thesis_pad_generate_messages_py)

# add dependencies to all check dependencies targets
get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/LoginName.msg" NAME_WE)
add_dependencies(thesis_pad_generate_messages_py _thesis_pad_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/srv/RoomAddressee.srv" NAME_WE)
add_dependencies(thesis_pad_generate_messages_py _thesis_pad_generate_messages_check_deps_${_filename})
get_filename_component(_filename "/home/flavio/progetto_tesi_2021_22/src/thesis_pad/msg/Comunication.msg" NAME_WE)
add_dependencies(thesis_pad_generate_messages_py _thesis_pad_generate_messages_check_deps_${_filename})

# target for backward compatibility
add_custom_target(thesis_pad_genpy)
add_dependencies(thesis_pad_genpy thesis_pad_generate_messages_py)

# register target for catkin_package(EXPORTED_TARGETS)
list(APPEND ${PROJECT_NAME}_EXPORTED_TARGETS thesis_pad_generate_messages_py)



if(gencpp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/thesis_pad)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gencpp_INSTALL_DIR}/thesis_pad
    DESTINATION ${gencpp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_cpp)
  add_dependencies(thesis_pad_generate_messages_cpp std_msgs_generate_messages_cpp)
endif()

if(geneus_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/thesis_pad)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${geneus_INSTALL_DIR}/thesis_pad
    DESTINATION ${geneus_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_eus)
  add_dependencies(thesis_pad_generate_messages_eus std_msgs_generate_messages_eus)
endif()

if(genlisp_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/thesis_pad)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genlisp_INSTALL_DIR}/thesis_pad
    DESTINATION ${genlisp_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_lisp)
  add_dependencies(thesis_pad_generate_messages_lisp std_msgs_generate_messages_lisp)
endif()

if(gennodejs_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/thesis_pad)
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${gennodejs_INSTALL_DIR}/thesis_pad
    DESTINATION ${gennodejs_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_nodejs)
  add_dependencies(thesis_pad_generate_messages_nodejs std_msgs_generate_messages_nodejs)
endif()

if(genpy_INSTALL_DIR AND EXISTS ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/thesis_pad)
  install(CODE "execute_process(COMMAND \"/usr/bin/python2\" -m compileall \"${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/thesis_pad\")")
  # install generated code
  install(
    DIRECTORY ${CATKIN_DEVEL_PREFIX}/${genpy_INSTALL_DIR}/thesis_pad
    DESTINATION ${genpy_INSTALL_DIR}
  )
endif()
if(TARGET std_msgs_generate_messages_py)
  add_dependencies(thesis_pad_generate_messages_py std_msgs_generate_messages_py)
endif()
