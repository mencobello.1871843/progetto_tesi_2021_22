; Auto-generated. Do not edit!


(cl:in-package pick_and_delivery-msg)


;//! \htmlinclude Comunication.msg.html

(cl:defclass <Comunication> (roslisp-msg-protocol:ros-message)
  ((sender
    :reader sender
    :initarg :sender
    :type cl:string
    :initform "")
   (addressee
    :reader addressee
    :initarg :addressee
    :type cl:string
    :initform "")
   (taken
    :reader taken
    :initarg :taken
    :type cl:boolean
    :initform cl:nil)
   (arrived
    :reader arrived
    :initarg :arrived
    :type cl:boolean
    :initform cl:nil)
   (failed
    :reader failed
    :initarg :failed
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass Comunication (<Comunication>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Comunication>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Comunication)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name pick_and_delivery-msg:<Comunication> is deprecated: use pick_and_delivery-msg:Comunication instead.")))

(cl:ensure-generic-function 'sender-val :lambda-list '(m))
(cl:defmethod sender-val ((m <Comunication>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pick_and_delivery-msg:sender-val is deprecated.  Use pick_and_delivery-msg:sender instead.")
  (sender m))

(cl:ensure-generic-function 'addressee-val :lambda-list '(m))
(cl:defmethod addressee-val ((m <Comunication>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pick_and_delivery-msg:addressee-val is deprecated.  Use pick_and_delivery-msg:addressee instead.")
  (addressee m))

(cl:ensure-generic-function 'taken-val :lambda-list '(m))
(cl:defmethod taken-val ((m <Comunication>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pick_and_delivery-msg:taken-val is deprecated.  Use pick_and_delivery-msg:taken instead.")
  (taken m))

(cl:ensure-generic-function 'arrived-val :lambda-list '(m))
(cl:defmethod arrived-val ((m <Comunication>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pick_and_delivery-msg:arrived-val is deprecated.  Use pick_and_delivery-msg:arrived instead.")
  (arrived m))

(cl:ensure-generic-function 'failed-val :lambda-list '(m))
(cl:defmethod failed-val ((m <Comunication>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pick_and_delivery-msg:failed-val is deprecated.  Use pick_and_delivery-msg:failed instead.")
  (failed m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Comunication>) ostream)
  "Serializes a message object of type '<Comunication>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'sender))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'sender))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'addressee))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'addressee))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'taken) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'arrived) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'failed) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Comunication>) istream)
  "Deserializes a message object of type '<Comunication>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'sender) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'sender) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'addressee) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'addressee) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:setf (cl:slot-value msg 'taken) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'arrived) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'failed) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Comunication>)))
  "Returns string type for a message object of type '<Comunication>"
  "pick_and_delivery/Comunication")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Comunication)))
  "Returns string type for a message object of type 'Comunication"
  "pick_and_delivery/Comunication")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Comunication>)))
  "Returns md5sum for a message object of type '<Comunication>"
  "188732a570db7a36788ed44270995502")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Comunication)))
  "Returns md5sum for a message object of type 'Comunication"
  "188732a570db7a36788ed44270995502")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Comunication>)))
  "Returns full string definition for message of type '<Comunication>"
  (cl:format cl:nil "string sender~%string addressee~%bool taken~%bool arrived~%bool failed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Comunication)))
  "Returns full string definition for message of type 'Comunication"
  (cl:format cl:nil "string sender~%string addressee~%bool taken~%bool arrived~%bool failed~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Comunication>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'sender))
     4 (cl:length (cl:slot-value msg 'addressee))
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Comunication>))
  "Converts a ROS message object to a list"
  (cl:list 'Comunication
    (cl:cons ':sender (sender msg))
    (cl:cons ':addressee (addressee msg))
    (cl:cons ':taken (taken msg))
    (cl:cons ':arrived (arrived msg))
    (cl:cons ':failed (failed msg))
))
