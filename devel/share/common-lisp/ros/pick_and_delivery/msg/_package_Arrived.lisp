(cl:in-package pick_and_delivery-msg)
(cl:export '(NAME-VAL
          NAME
          ARRIVED-VAL
          ARRIVED
          QUIT-VAL
          QUIT
          OCCUPIED-VAL
          OCCUPIED
))