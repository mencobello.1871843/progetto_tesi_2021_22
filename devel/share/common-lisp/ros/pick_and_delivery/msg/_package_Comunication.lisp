(cl:in-package pick_and_delivery-msg)
(cl:export '(SENDER-VAL
          SENDER
          ADDRESSEE-VAL
          ADDRESSEE
          TAKEN-VAL
          TAKEN
          ARRIVED-VAL
          ARRIVED
          FAILED-VAL
          FAILED
))