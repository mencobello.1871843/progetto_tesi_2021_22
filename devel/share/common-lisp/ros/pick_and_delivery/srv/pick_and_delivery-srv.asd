
(cl:in-package :asdf)

(defsystem "pick_and_delivery-srv"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "RoomAddressee" :depends-on ("_package_RoomAddressee"))
    (:file "_package_RoomAddressee" :depends-on ("_package"))
  ))