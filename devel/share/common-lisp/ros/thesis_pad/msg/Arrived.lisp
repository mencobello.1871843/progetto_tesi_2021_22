; Auto-generated. Do not edit!


(cl:in-package thesis_pad-msg)


;//! \htmlinclude Arrived.msg.html

(cl:defclass <Arrived> (roslisp-msg-protocol:ros-message)
  ((name
    :reader name
    :initarg :name
    :type cl:string
    :initform "")
   (arrived
    :reader arrived
    :initarg :arrived
    :type cl:boolean
    :initform cl:nil)
   (quit
    :reader quit
    :initarg :quit
    :type cl:boolean
    :initform cl:nil)
   (occupied
    :reader occupied
    :initarg :occupied
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass Arrived (<Arrived>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <Arrived>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'Arrived)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name thesis_pad-msg:<Arrived> is deprecated: use thesis_pad-msg:Arrived instead.")))

(cl:ensure-generic-function 'name-val :lambda-list '(m))
(cl:defmethod name-val ((m <Arrived>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thesis_pad-msg:name-val is deprecated.  Use thesis_pad-msg:name instead.")
  (name m))

(cl:ensure-generic-function 'arrived-val :lambda-list '(m))
(cl:defmethod arrived-val ((m <Arrived>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thesis_pad-msg:arrived-val is deprecated.  Use thesis_pad-msg:arrived instead.")
  (arrived m))

(cl:ensure-generic-function 'quit-val :lambda-list '(m))
(cl:defmethod quit-val ((m <Arrived>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thesis_pad-msg:quit-val is deprecated.  Use thesis_pad-msg:quit instead.")
  (quit m))

(cl:ensure-generic-function 'occupied-val :lambda-list '(m))
(cl:defmethod occupied-val ((m <Arrived>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thesis_pad-msg:occupied-val is deprecated.  Use thesis_pad-msg:occupied instead.")
  (occupied m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <Arrived>) ostream)
  "Serializes a message object of type '<Arrived>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'name))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'arrived) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'quit) 1 0)) ostream)
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'occupied) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <Arrived>) istream)
  "Deserializes a message object of type '<Arrived>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:setf (cl:slot-value msg 'arrived) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'quit) (cl:not (cl:zerop (cl:read-byte istream))))
    (cl:setf (cl:slot-value msg 'occupied) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<Arrived>)))
  "Returns string type for a message object of type '<Arrived>"
  "thesis_pad/Arrived")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'Arrived)))
  "Returns string type for a message object of type 'Arrived"
  "thesis_pad/Arrived")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<Arrived>)))
  "Returns md5sum for a message object of type '<Arrived>"
  "28548b496855d702abe422d706730c3e")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'Arrived)))
  "Returns md5sum for a message object of type 'Arrived"
  "28548b496855d702abe422d706730c3e")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<Arrived>)))
  "Returns full string definition for message of type '<Arrived>"
  (cl:format cl:nil "string name~%bool arrived~%bool quit~%bool occupied~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'Arrived)))
  "Returns full string definition for message of type 'Arrived"
  (cl:format cl:nil "string name~%bool arrived~%bool quit~%bool occupied~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <Arrived>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'name))
     1
     1
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <Arrived>))
  "Converts a ROS message object to a list"
  (cl:list 'Arrived
    (cl:cons ':name (name msg))
    (cl:cons ':arrived (arrived msg))
    (cl:cons ':quit (quit msg))
    (cl:cons ':occupied (occupied msg))
))
