(cl:in-package thesis_pad-msg)
(cl:export '(NAME-VAL
          NAME
          ARRIVED-VAL
          ARRIVED
          QUIT-VAL
          QUIT
          OCCUPIED-VAL
          OCCUPIED
))