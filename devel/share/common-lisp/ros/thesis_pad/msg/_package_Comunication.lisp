(cl:in-package thesis_pad-msg)
(cl:export '(SENDER-VAL
          SENDER
          ADDRESSEE-VAL
          ADDRESSEE
          R_NUM-VAL
          R_NUM
          TAKEN-VAL
          TAKEN
          ARRIVED-VAL
          ARRIVED
          FAILED-VAL
          FAILED
))