
(cl:in-package :asdf)

(defsystem "thesis_pad-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Addressee" :depends-on ("_package_Addressee"))
    (:file "_package_Addressee" :depends-on ("_package"))
    (:file "Arrived" :depends-on ("_package_Arrived"))
    (:file "_package_Arrived" :depends-on ("_package"))
    (:file "Comunication" :depends-on ("_package_Comunication"))
    (:file "_package_Comunication" :depends-on ("_package"))
    (:file "LoginName" :depends-on ("_package_LoginName"))
    (:file "_package_LoginName" :depends-on ("_package"))
    (:file "Taken" :depends-on ("_package_Taken"))
    (:file "_package_Taken" :depends-on ("_package"))
    (:file "WorkingFor" :depends-on ("_package_WorkingFor"))
    (:file "_package_WorkingFor" :depends-on ("_package"))
  ))