; Auto-generated. Do not edit!


(cl:in-package thesis_pad-srv)


;//! \htmlinclude RoomAddressee-request.msg.html

(cl:defclass <RoomAddressee-request> (roslisp-msg-protocol:ros-message)
  ((x
    :reader x
    :initarg :x
    :type cl:float
    :initform 0.0)
   (y
    :reader y
    :initarg :y
    :type cl:float
    :initform 0.0)
   (theta
    :reader theta
    :initarg :theta
    :type cl:float
    :initform 0.0)
   (room_addr
    :reader room_addr
    :initarg :room_addr
    :type cl:string
    :initform "")
   (room_send
    :reader room_send
    :initarg :room_send
    :type cl:string
    :initform "")
   (room_command
    :reader room_command
    :initarg :room_command
    :type cl:string
    :initform "")
   (r_num
    :reader r_num
    :initarg :r_num
    :type cl:integer
    :initform 0))
)

(cl:defclass RoomAddressee-request (<RoomAddressee-request>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <RoomAddressee-request>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'RoomAddressee-request)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name thesis_pad-srv:<RoomAddressee-request> is deprecated: use thesis_pad-srv:RoomAddressee-request instead.")))

(cl:ensure-generic-function 'x-val :lambda-list '(m))
(cl:defmethod x-val ((m <RoomAddressee-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thesis_pad-srv:x-val is deprecated.  Use thesis_pad-srv:x instead.")
  (x m))

(cl:ensure-generic-function 'y-val :lambda-list '(m))
(cl:defmethod y-val ((m <RoomAddressee-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thesis_pad-srv:y-val is deprecated.  Use thesis_pad-srv:y instead.")
  (y m))

(cl:ensure-generic-function 'theta-val :lambda-list '(m))
(cl:defmethod theta-val ((m <RoomAddressee-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thesis_pad-srv:theta-val is deprecated.  Use thesis_pad-srv:theta instead.")
  (theta m))

(cl:ensure-generic-function 'room_addr-val :lambda-list '(m))
(cl:defmethod room_addr-val ((m <RoomAddressee-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thesis_pad-srv:room_addr-val is deprecated.  Use thesis_pad-srv:room_addr instead.")
  (room_addr m))

(cl:ensure-generic-function 'room_send-val :lambda-list '(m))
(cl:defmethod room_send-val ((m <RoomAddressee-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thesis_pad-srv:room_send-val is deprecated.  Use thesis_pad-srv:room_send instead.")
  (room_send m))

(cl:ensure-generic-function 'room_command-val :lambda-list '(m))
(cl:defmethod room_command-val ((m <RoomAddressee-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thesis_pad-srv:room_command-val is deprecated.  Use thesis_pad-srv:room_command instead.")
  (room_command m))

(cl:ensure-generic-function 'r_num-val :lambda-list '(m))
(cl:defmethod r_num-val ((m <RoomAddressee-request>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thesis_pad-srv:r_num-val is deprecated.  Use thesis_pad-srv:r_num instead.")
  (r_num m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <RoomAddressee-request>) ostream)
  "Serializes a message object of type '<RoomAddressee-request>"
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'x))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'y))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'theta))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'room_addr))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'room_addr))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'room_send))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'room_send))
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'room_command))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'room_command))
  (cl:let* ((signed (cl:slot-value msg 'r_num)) (unsigned (cl:if (cl:< signed 0) (cl:+ signed 18446744073709551616) signed)))
    (cl:write-byte (cl:ldb (cl:byte 8 0) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 32) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 40) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 48) unsigned) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 56) unsigned) ostream)
    )
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <RoomAddressee-request>) istream)
  "Deserializes a message object of type '<RoomAddressee-request>"
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'x) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'y) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'theta) (roslisp-utils:decode-single-float-bits bits)))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'room_addr) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'room_addr) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'room_send) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'room_send) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'room_command) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'room_command) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((unsigned 0))
      (cl:setf (cl:ldb (cl:byte 8 0) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 32) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 40) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 48) unsigned) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 56) unsigned) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'r_num) (cl:if (cl:< unsigned 9223372036854775808) unsigned (cl:- unsigned 18446744073709551616))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<RoomAddressee-request>)))
  "Returns string type for a service object of type '<RoomAddressee-request>"
  "thesis_pad/RoomAddresseeRequest")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RoomAddressee-request)))
  "Returns string type for a service object of type 'RoomAddressee-request"
  "thesis_pad/RoomAddresseeRequest")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<RoomAddressee-request>)))
  "Returns md5sum for a message object of type '<RoomAddressee-request>"
  "bdd55c5228f8ef0eed942cf5e76cf0db")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'RoomAddressee-request)))
  "Returns md5sum for a message object of type 'RoomAddressee-request"
  "bdd55c5228f8ef0eed942cf5e76cf0db")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<RoomAddressee-request>)))
  "Returns full string definition for message of type '<RoomAddressee-request>"
  (cl:format cl:nil "float32 x~%float32 y~%float32 theta~%string room_addr~%string room_send~%string room_command~%int64 r_num~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'RoomAddressee-request)))
  "Returns full string definition for message of type 'RoomAddressee-request"
  (cl:format cl:nil "float32 x~%float32 y~%float32 theta~%string room_addr~%string room_send~%string room_command~%int64 r_num~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <RoomAddressee-request>))
  (cl:+ 0
     4
     4
     4
     4 (cl:length (cl:slot-value msg 'room_addr))
     4 (cl:length (cl:slot-value msg 'room_send))
     4 (cl:length (cl:slot-value msg 'room_command))
     8
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <RoomAddressee-request>))
  "Converts a ROS message object to a list"
  (cl:list 'RoomAddressee-request
    (cl:cons ':x (x msg))
    (cl:cons ':y (y msg))
    (cl:cons ':theta (theta msg))
    (cl:cons ':room_addr (room_addr msg))
    (cl:cons ':room_send (room_send msg))
    (cl:cons ':room_command (room_command msg))
    (cl:cons ':r_num (r_num msg))
))
;//! \htmlinclude RoomAddressee-response.msg.html

(cl:defclass <RoomAddressee-response> (roslisp-msg-protocol:ros-message)
  ((response
    :reader response
    :initarg :response
    :type cl:string
    :initform ""))
)

(cl:defclass RoomAddressee-response (<RoomAddressee-response>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <RoomAddressee-response>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'RoomAddressee-response)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name thesis_pad-srv:<RoomAddressee-response> is deprecated: use thesis_pad-srv:RoomAddressee-response instead.")))

(cl:ensure-generic-function 'response-val :lambda-list '(m))
(cl:defmethod response-val ((m <RoomAddressee-response>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader thesis_pad-srv:response-val is deprecated.  Use thesis_pad-srv:response instead.")
  (response m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <RoomAddressee-response>) ostream)
  "Serializes a message object of type '<RoomAddressee-response>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'response))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'response))
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <RoomAddressee-response>) istream)
  "Deserializes a message object of type '<RoomAddressee-response>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'response) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'response) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<RoomAddressee-response>)))
  "Returns string type for a service object of type '<RoomAddressee-response>"
  "thesis_pad/RoomAddresseeResponse")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RoomAddressee-response)))
  "Returns string type for a service object of type 'RoomAddressee-response"
  "thesis_pad/RoomAddresseeResponse")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<RoomAddressee-response>)))
  "Returns md5sum for a message object of type '<RoomAddressee-response>"
  "bdd55c5228f8ef0eed942cf5e76cf0db")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'RoomAddressee-response)))
  "Returns md5sum for a message object of type 'RoomAddressee-response"
  "bdd55c5228f8ef0eed942cf5e76cf0db")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<RoomAddressee-response>)))
  "Returns full string definition for message of type '<RoomAddressee-response>"
  (cl:format cl:nil "string response~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'RoomAddressee-response)))
  "Returns full string definition for message of type 'RoomAddressee-response"
  (cl:format cl:nil "string response~%~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <RoomAddressee-response>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'response))
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <RoomAddressee-response>))
  "Converts a ROS message object to a list"
  (cl:list 'RoomAddressee-response
    (cl:cons ':response (response msg))
))
(cl:defmethod roslisp-msg-protocol:service-request-type ((msg (cl:eql 'RoomAddressee)))
  'RoomAddressee-request)
(cl:defmethod roslisp-msg-protocol:service-response-type ((msg (cl:eql 'RoomAddressee)))
  'RoomAddressee-response)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'RoomAddressee)))
  "Returns string type for a service object of type '<RoomAddressee>"
  "thesis_pad/RoomAddressee")