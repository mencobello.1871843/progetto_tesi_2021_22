(cl:in-package thesis_pad-srv)
(cl:export '(X-VAL
          X
          Y-VAL
          Y
          THETA-VAL
          THETA
          ROOM_ADDR-VAL
          ROOM_ADDR
          ROOM_SEND-VAL
          ROOM_SEND
          ROOM_COMMAND-VAL
          ROOM_COMMAND
          R_NUM-VAL
          R_NUM
          RESPONSE-VAL
          RESPONSE
))