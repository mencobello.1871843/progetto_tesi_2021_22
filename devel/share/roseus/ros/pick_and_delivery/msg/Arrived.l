;; Auto-generated. Do not edit!


(when (boundp 'pick_and_delivery::Arrived)
  (if (not (find-package "PICK_AND_DELIVERY"))
    (make-package "PICK_AND_DELIVERY"))
  (shadow 'Arrived (find-package "PICK_AND_DELIVERY")))
(unless (find-package "PICK_AND_DELIVERY::ARRIVED")
  (make-package "PICK_AND_DELIVERY::ARRIVED"))

(in-package "ROS")
;;//! \htmlinclude Arrived.msg.html


(defclass pick_and_delivery::Arrived
  :super ros::object
  :slots (_name _arrived _quit _occupied ))

(defmethod pick_and_delivery::Arrived
  (:init
   (&key
    ((:name __name) "")
    ((:arrived __arrived) nil)
    ((:quit __quit) nil)
    ((:occupied __occupied) nil)
    )
   (send-super :init)
   (setq _name (string __name))
   (setq _arrived __arrived)
   (setq _quit __quit)
   (setq _occupied __occupied)
   self)
  (:name
   (&optional __name)
   (if __name (setq _name __name)) _name)
  (:arrived
   (&optional __arrived)
   (if __arrived (setq _arrived __arrived)) _arrived)
  (:quit
   (&optional __quit)
   (if __quit (setq _quit __quit)) _quit)
  (:occupied
   (&optional __occupied)
   (if __occupied (setq _occupied __occupied)) _occupied)
  (:serialization-length
   ()
   (+
    ;; string _name
    4 (length _name)
    ;; bool _arrived
    1
    ;; bool _quit
    1
    ;; bool _occupied
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _name
       (write-long (length _name) s) (princ _name s)
     ;; bool _arrived
       (if _arrived (write-byte -1 s) (write-byte 0 s))
     ;; bool _quit
       (if _quit (write-byte -1 s) (write-byte 0 s))
     ;; bool _occupied
       (if _occupied (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; bool _arrived
     (setq _arrived (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _quit
     (setq _quit (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _occupied
     (setq _occupied (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get pick_and_delivery::Arrived :md5sum-) "28548b496855d702abe422d706730c3e")
(setf (get pick_and_delivery::Arrived :datatype-) "pick_and_delivery/Arrived")
(setf (get pick_and_delivery::Arrived :definition-)
      "string name
bool arrived
bool quit
bool occupied

")



(provide :pick_and_delivery/Arrived "28548b496855d702abe422d706730c3e")


