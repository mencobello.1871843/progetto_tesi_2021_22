;; Auto-generated. Do not edit!


(when (boundp 'pick_and_delivery::LoginName)
  (if (not (find-package "PICK_AND_DELIVERY"))
    (make-package "PICK_AND_DELIVERY"))
  (shadow 'LoginName (find-package "PICK_AND_DELIVERY")))
(unless (find-package "PICK_AND_DELIVERY::LOGINNAME")
  (make-package "PICK_AND_DELIVERY::LOGINNAME"))

(in-package "ROS")
;;//! \htmlinclude LoginName.msg.html


(defclass pick_and_delivery::LoginName
  :super ros::object
  :slots (_name _log ))

(defmethod pick_and_delivery::LoginName
  (:init
   (&key
    ((:name __name) "")
    ((:log __log) nil)
    )
   (send-super :init)
   (setq _name (string __name))
   (setq _log __log)
   self)
  (:name
   (&optional __name)
   (if __name (setq _name __name)) _name)
  (:log
   (&optional __log)
   (if __log (setq _log __log)) _log)
  (:serialization-length
   ()
   (+
    ;; string _name
    4 (length _name)
    ;; bool _log
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _name
       (write-long (length _name) s) (princ _name s)
     ;; bool _log
       (if _log (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; bool _log
     (setq _log (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get pick_and_delivery::LoginName :md5sum-) "6949c869b9f974ee2e8f6741c95510d2")
(setf (get pick_and_delivery::LoginName :datatype-) "pick_and_delivery/LoginName")
(setf (get pick_and_delivery::LoginName :definition-)
      "string name
bool log

")



(provide :pick_and_delivery/LoginName "6949c869b9f974ee2e8f6741c95510d2")


