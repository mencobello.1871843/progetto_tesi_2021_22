;; Auto-generated. Do not edit!


(when (boundp 'pick_and_delivery::Taken)
  (if (not (find-package "PICK_AND_DELIVERY"))
    (make-package "PICK_AND_DELIVERY"))
  (shadow 'Taken (find-package "PICK_AND_DELIVERY")))
(unless (find-package "PICK_AND_DELIVERY::TAKEN")
  (make-package "PICK_AND_DELIVERY::TAKEN"))

(in-package "ROS")
;;//! \htmlinclude Taken.msg.html


(defclass pick_and_delivery::Taken
  :super ros::object
  :slots (_name _taken ))

(defmethod pick_and_delivery::Taken
  (:init
   (&key
    ((:name __name) "")
    ((:taken __taken) nil)
    )
   (send-super :init)
   (setq _name (string __name))
   (setq _taken __taken)
   self)
  (:name
   (&optional __name)
   (if __name (setq _name __name)) _name)
  (:taken
   (&optional __taken)
   (if __taken (setq _taken __taken)) _taken)
  (:serialization-length
   ()
   (+
    ;; string _name
    4 (length _name)
    ;; bool _taken
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _name
       (write-long (length _name) s) (princ _name s)
     ;; bool _taken
       (if _taken (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; bool _taken
     (setq _taken (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get pick_and_delivery::Taken :md5sum-) "2c7320311e09f8bf62ab483e21f3592d")
(setf (get pick_and_delivery::Taken :datatype-) "pick_and_delivery/Taken")
(setf (get pick_and_delivery::Taken :definition-)
      "string name
bool taken

")



(provide :pick_and_delivery/Taken "2c7320311e09f8bf62ab483e21f3592d")


