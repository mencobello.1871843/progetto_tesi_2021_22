;; Auto-generated. Do not edit!


(when (boundp 'thesis_pad::Comunication)
  (if (not (find-package "THESIS_PAD"))
    (make-package "THESIS_PAD"))
  (shadow 'Comunication (find-package "THESIS_PAD")))
(unless (find-package "THESIS_PAD::COMUNICATION")
  (make-package "THESIS_PAD::COMUNICATION"))

(in-package "ROS")
;;//! \htmlinclude Comunication.msg.html


(defclass thesis_pad::Comunication
  :super ros::object
  :slots (_sender _addressee _r_num _taken _arrived _failed ))

(defmethod thesis_pad::Comunication
  (:init
   (&key
    ((:sender __sender) "")
    ((:addressee __addressee) "")
    ((:r_num __r_num) 0)
    ((:taken __taken) nil)
    ((:arrived __arrived) nil)
    ((:failed __failed) nil)
    )
   (send-super :init)
   (setq _sender (string __sender))
   (setq _addressee (string __addressee))
   (setq _r_num (round __r_num))
   (setq _taken __taken)
   (setq _arrived __arrived)
   (setq _failed __failed)
   self)
  (:sender
   (&optional __sender)
   (if __sender (setq _sender __sender)) _sender)
  (:addressee
   (&optional __addressee)
   (if __addressee (setq _addressee __addressee)) _addressee)
  (:r_num
   (&optional __r_num)
   (if __r_num (setq _r_num __r_num)) _r_num)
  (:taken
   (&optional __taken)
   (if __taken (setq _taken __taken)) _taken)
  (:arrived
   (&optional __arrived)
   (if __arrived (setq _arrived __arrived)) _arrived)
  (:failed
   (&optional __failed)
   (if __failed (setq _failed __failed)) _failed)
  (:serialization-length
   ()
   (+
    ;; string _sender
    4 (length _sender)
    ;; string _addressee
    4 (length _addressee)
    ;; int64 _r_num
    8
    ;; bool _taken
    1
    ;; bool _arrived
    1
    ;; bool _failed
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _sender
       (write-long (length _sender) s) (princ _sender s)
     ;; string _addressee
       (write-long (length _addressee) s) (princ _addressee s)
     ;; int64 _r_num
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _r_num (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _r_num) (= (length (_r_num . bv)) 2)) ;; bignum
              (write-long (ash (elt (_r_num . bv) 0) 0) s)
              (write-long (ash (elt (_r_num . bv) 1) -1) s))
             ((and (class _r_num) (= (length (_r_num . bv)) 1)) ;; big1
              (write-long (elt (_r_num . bv) 0) s)
              (write-long (if (>= _r_num 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _r_num s)(write-long (if (>= _r_num 0) 0 #xffffffff) s)))
     ;; bool _taken
       (if _taken (write-byte -1 s) (write-byte 0 s))
     ;; bool _arrived
       (if _arrived (write-byte -1 s) (write-byte 0 s))
     ;; bool _failed
       (if _failed (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _sender
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _sender (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _addressee
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _addressee (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; int64 _r_num
#+(or :alpha :irix6 :x86_64)
      (setf _r_num (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _r_num (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;; bool _taken
     (setq _taken (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _arrived
     (setq _arrived (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _failed
     (setq _failed (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get thesis_pad::Comunication :md5sum-) "3869228eba3216950749fcb9838c091b")
(setf (get thesis_pad::Comunication :datatype-) "thesis_pad/Comunication")
(setf (get thesis_pad::Comunication :definition-)
      "string sender
string addressee
int64 r_num
bool taken
bool arrived
bool failed

")



(provide :thesis_pad/Comunication "3869228eba3216950749fcb9838c091b")


