;; Auto-generated. Do not edit!


(when (boundp 'thesis_pad::LoginName)
  (if (not (find-package "THESIS_PAD"))
    (make-package "THESIS_PAD"))
  (shadow 'LoginName (find-package "THESIS_PAD")))
(unless (find-package "THESIS_PAD::LOGINNAME")
  (make-package "THESIS_PAD::LOGINNAME"))

(in-package "ROS")
;;//! \htmlinclude LoginName.msg.html


(defclass thesis_pad::LoginName
  :super ros::object
  :slots (_name _log ))

(defmethod thesis_pad::LoginName
  (:init
   (&key
    ((:name __name) "")
    ((:log __log) nil)
    )
   (send-super :init)
   (setq _name (string __name))
   (setq _log __log)
   self)
  (:name
   (&optional __name)
   (if __name (setq _name __name)) _name)
  (:log
   (&optional __log)
   (if __log (setq _log __log)) _log)
  (:serialization-length
   ()
   (+
    ;; string _name
    4 (length _name)
    ;; bool _log
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _name
       (write-long (length _name) s) (princ _name s)
     ;; bool _log
       (if _log (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; bool _log
     (setq _log (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get thesis_pad::LoginName :md5sum-) "6949c869b9f974ee2e8f6741c95510d2")
(setf (get thesis_pad::LoginName :datatype-) "thesis_pad/LoginName")
(setf (get thesis_pad::LoginName :definition-)
      "string name
bool log

")



(provide :thesis_pad/LoginName "6949c869b9f974ee2e8f6741c95510d2")


