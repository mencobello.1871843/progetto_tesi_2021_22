;; Auto-generated. Do not edit!


(when (boundp 'thesis_pad::RoomAddressee)
  (if (not (find-package "THESIS_PAD"))
    (make-package "THESIS_PAD"))
  (shadow 'RoomAddressee (find-package "THESIS_PAD")))
(unless (find-package "THESIS_PAD::ROOMADDRESSEE")
  (make-package "THESIS_PAD::ROOMADDRESSEE"))
(unless (find-package "THESIS_PAD::ROOMADDRESSEEREQUEST")
  (make-package "THESIS_PAD::ROOMADDRESSEEREQUEST"))
(unless (find-package "THESIS_PAD::ROOMADDRESSEERESPONSE")
  (make-package "THESIS_PAD::ROOMADDRESSEERESPONSE"))

(in-package "ROS")





(defclass thesis_pad::RoomAddresseeRequest
  :super ros::object
  :slots (_x _y _theta _room_addr _room_send _room_command _r_num ))

(defmethod thesis_pad::RoomAddresseeRequest
  (:init
   (&key
    ((:x __x) 0.0)
    ((:y __y) 0.0)
    ((:theta __theta) 0.0)
    ((:room_addr __room_addr) "")
    ((:room_send __room_send) "")
    ((:room_command __room_command) "")
    ((:r_num __r_num) 0)
    )
   (send-super :init)
   (setq _x (float __x))
   (setq _y (float __y))
   (setq _theta (float __theta))
   (setq _room_addr (string __room_addr))
   (setq _room_send (string __room_send))
   (setq _room_command (string __room_command))
   (setq _r_num (round __r_num))
   self)
  (:x
   (&optional __x)
   (if __x (setq _x __x)) _x)
  (:y
   (&optional __y)
   (if __y (setq _y __y)) _y)
  (:theta
   (&optional __theta)
   (if __theta (setq _theta __theta)) _theta)
  (:room_addr
   (&optional __room_addr)
   (if __room_addr (setq _room_addr __room_addr)) _room_addr)
  (:room_send
   (&optional __room_send)
   (if __room_send (setq _room_send __room_send)) _room_send)
  (:room_command
   (&optional __room_command)
   (if __room_command (setq _room_command __room_command)) _room_command)
  (:r_num
   (&optional __r_num)
   (if __r_num (setq _r_num __r_num)) _r_num)
  (:serialization-length
   ()
   (+
    ;; float32 _x
    4
    ;; float32 _y
    4
    ;; float32 _theta
    4
    ;; string _room_addr
    4 (length _room_addr)
    ;; string _room_send
    4 (length _room_send)
    ;; string _room_command
    4 (length _room_command)
    ;; int64 _r_num
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _x
       (sys::poke _x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _y
       (sys::poke _y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _theta
       (sys::poke _theta (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; string _room_addr
       (write-long (length _room_addr) s) (princ _room_addr s)
     ;; string _room_send
       (write-long (length _room_send) s) (princ _room_send s)
     ;; string _room_command
       (write-long (length _room_command) s) (princ _room_command s)
     ;; int64 _r_num
#+(or :alpha :irix6 :x86_64)
       (progn (sys::poke _r_num (send s :buffer) (send s :count) :long) (incf (stream-count s) 8))
#-(or :alpha :irix6 :x86_64)
       (cond ((and (class _r_num) (= (length (_r_num . bv)) 2)) ;; bignum
              (write-long (ash (elt (_r_num . bv) 0) 0) s)
              (write-long (ash (elt (_r_num . bv) 1) -1) s))
             ((and (class _r_num) (= (length (_r_num . bv)) 1)) ;; big1
              (write-long (elt (_r_num . bv) 0) s)
              (write-long (if (>= _r_num 0) 0 #xffffffff) s))
             (t                                         ;; integer
              (write-long _r_num s)(write-long (if (>= _r_num 0) 0 #xffffffff) s)))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _x
     (setq _x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _y
     (setq _y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _theta
     (setq _theta (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; string _room_addr
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _room_addr (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _room_send
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _room_send (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _room_command
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _room_command (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; int64 _r_num
#+(or :alpha :irix6 :x86_64)
      (setf _r_num (prog1 (sys::peek buf ptr- :long) (incf ptr- 8)))
#-(or :alpha :irix6 :x86_64)
      (setf _r_num (let ((b0 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4)))
                  (b1 (prog1 (sys::peek buf ptr- :integer) (incf ptr- 4))))
              (cond ((= b1 -1) b0)
                     ((and (= b1  0)
                           (<= lisp::most-negative-fixnum b0 lisp::most-positive-fixnum))
                      b0)
                    ((= b1  0) (make-instance bignum :size 1 :bv (integer-vector b0)))
                    (t (make-instance bignum :size 2 :bv (integer-vector b0 (ash b1 1)))))))
   ;;
   self)
  )

(defclass thesis_pad::RoomAddresseeResponse
  :super ros::object
  :slots (_response ))

(defmethod thesis_pad::RoomAddresseeResponse
  (:init
   (&key
    ((:response __response) "")
    )
   (send-super :init)
   (setq _response (string __response))
   self)
  (:response
   (&optional __response)
   (if __response (setq _response __response)) _response)
  (:serialization-length
   ()
   (+
    ;; string _response
    4 (length _response)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _response
       (write-long (length _response) s) (princ _response s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _response
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _response (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass thesis_pad::RoomAddressee
  :super ros::object
  :slots ())

(setf (get thesis_pad::RoomAddressee :md5sum-) "bdd55c5228f8ef0eed942cf5e76cf0db")
(setf (get thesis_pad::RoomAddressee :datatype-) "thesis_pad/RoomAddressee")
(setf (get thesis_pad::RoomAddressee :request) thesis_pad::RoomAddresseeRequest)
(setf (get thesis_pad::RoomAddressee :response) thesis_pad::RoomAddresseeResponse)

(defmethod thesis_pad::RoomAddresseeRequest
  (:response () (instance thesis_pad::RoomAddresseeResponse :init)))

(setf (get thesis_pad::RoomAddresseeRequest :md5sum-) "bdd55c5228f8ef0eed942cf5e76cf0db")
(setf (get thesis_pad::RoomAddresseeRequest :datatype-) "thesis_pad/RoomAddresseeRequest")
(setf (get thesis_pad::RoomAddresseeRequest :definition-)
      "float32 x
float32 y
float32 theta
string room_addr
string room_send
string room_command
int64 r_num
---
string response

")

(setf (get thesis_pad::RoomAddresseeResponse :md5sum-) "bdd55c5228f8ef0eed942cf5e76cf0db")
(setf (get thesis_pad::RoomAddresseeResponse :datatype-) "thesis_pad/RoomAddresseeResponse")
(setf (get thesis_pad::RoomAddresseeResponse :definition-)
      "float32 x
float32 y
float32 theta
string room_addr
string room_send
string room_command
int64 r_num
---
string response

")



(provide :thesis_pad/RoomAddressee "bdd55c5228f8ef0eed942cf5e76cf0db")


