#include "ros/ros.h"
#include <vector>
#include <sstream>
#include <thread>
#include <chrono>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/TransformStamped.h"
#include "geometry_msgs/Twist.h"
#include "tf/tf.h"
#include "tf2_ros/transform_listener.h"
#include "tf2_msgs/TFMessage.h"
#include "thesis_pad/RoomAddressee.h"
#include "thesis_pad/LoginName.h"
#include "thesis_pad/Comunication.h"

tf2_ros::Buffer tfBuf;
std::vector<std::string> rooms;
int room_count = 0;
int check1 = 0;
int check2 = 0;
int find = 0;
int stuck0 = 0;
int stuck1 = 0;
int collisionResolution = 0;
std::vector<float> r0_addressee_position(2,0);
std::vector<float> r0_old_position(2,0);
std::vector<float> r0_d_comp(2,0);
std::vector<float> r0_actual_position(2,0);
std::vector<float> r0_base_position(2,0);
std::vector<float> r1_addressee_position(2,0);
std::vector<float> r1_old_position(2,0);
std::vector<float> r1_d_comp(2,0);
std::vector<float> r1_actual_position(2,0);
std::vector<float> r1_base_position(2,0);

enum state {available, cruiseToSender, sender, cruiseToAddressee, retToSender, addressee};
state s = state::available;

struct Robot{
	state st;
	std::string name;
	geometry_msgs::PoseStamped goal_msg;
	geometry_msgs::PoseStamped res_msg;
	int cruising;
	int publish;
	size_t m;
	std::string addr;
	std::string send;
	thesis_pad::Comunication com;
	int clientsPublish;
};

std::vector<Robot> robots;
Robot robot_0, robot_1;

void roomCallback(const thesis_pad::LoginName& room){
	if(room.log){
		ROS_INFO("Room logged");
		rooms.push_back(room.name);
		room_count++;
		std::cout<<rooms[room_count-1]<<std::endl;
	}
}

void roomCheckout(std::string unlog_room){
	check1 = 0;
	while(check1<room_count){
		if(rooms[check1]==unlog_room){
			rooms.erase(rooms.begin()+check1);
			room_count--;
			ROS_INFO("Room unlogged");
			break;
		}
		check1++;
	}
	ROS_INFO("Remaining rooms");
	check1 = 0;
	while(check1<room_count){
		std::cout<<rooms[check1]<<std::endl;
		check1++;
	}
	if(!check1) std::cout<<"No rooms"<<std::endl;
}

void returnToBase(int robot_number){
	robots[robot_number].goal_msg.header.seq = robots[robot_number].m;
	robots[robot_number].m++;
	robots[robot_number].goal_msg.header.stamp = ros::Time::now();
	robots[robot_number].goal_msg.header.frame_id = "map";

	robots[robot_number].goal_msg.pose.position.z = 0;

	robots[robot_number].goal_msg.pose.orientation.x = 0;
	robots[robot_number].goal_msg.pose.orientation.y = 0;
	robots[robot_number].goal_msg.pose.orientation.z = 0;
	robots[robot_number].goal_msg.pose.orientation.w = 1;

	robots[robot_number].publish = 1;
	robots[robot_number].cruising = 1;

	if(robot_number == 0){
		robots[robot_number].goal_msg.pose.position.x = r0_base_position[0];
		robots[robot_number].goal_msg.pose.position.y = r0_base_position[1];
		r0_addressee_position[0] = robots[robot_number].goal_msg.pose.position.x;
		r0_addressee_position[1] = robots[robot_number].goal_msg.pose.position.y;
	} else {
		robots[robot_number].goal_msg.pose.position.x = r1_base_position[0];
		robots[robot_number].goal_msg.pose.position.y = r1_base_position[1];
		r1_addressee_position[0] = robots[robot_number].goal_msg.pose.position.x;
		r1_addressee_position[1] = robots[robot_number].goal_msg.pose.position.y;
	}
}

void returnToSender(int robot_number){
	robots[robot_number].goal_msg.header.seq = robots[robot_number].m;
	robots[robot_number].m++;
	robots[robot_number].goal_msg.header.stamp = ros::Time::now();
	robots[robot_number].goal_msg.header.frame_id = "map";

	if(robot_number==0){
		robots[robot_number].goal_msg.pose.position.x = r0_old_position[0];
		robots[robot_number].goal_msg.pose.position.y = r0_old_position[1];
	}else{
		robots[robot_number].goal_msg.pose.position.x = r1_old_position[0];
		robots[robot_number].goal_msg.pose.position.y = r1_old_position[1];
	}
	robots[robot_number].goal_msg.pose.position.z = 0;

	
	robots[robot_number].goal_msg.pose.orientation.x = 0;
	robots[robot_number].goal_msg.pose.orientation.y = 0;
	robots[robot_number].goal_msg.pose.orientation.z = 0;
	robots[robot_number].goal_msg.pose.orientation.w = 1;
	robots[robot_number].publish = 1;
	robots[robot_number].cruising = 1;

	if(robot_number == 0){
		r0_addressee_position[0] = robots[robot_number].goal_msg.pose.position.x;
		r0_addressee_position[1] = robots[robot_number].goal_msg.pose.position.y;
	} else {
		r1_addressee_position[0] = robots[robot_number].goal_msg.pose.position.x;
		r1_addressee_position[1] = robots[robot_number].goal_msg.pose.position.y;
	}
}

bool setAddresseeService(thesis_pad::RoomAddressee::Request& req, thesis_pad::RoomAddressee::Response& res){

	if(robots[req.r_num].st == state::available){
		if(req.room_command == "call"){
			if (req.room_send == robots[0].addr || req.room_send == robots[1].addr) res.response = "waitSender";
			else{
				res.response = "sender";
				robots[req.r_num].send = req.room_send;
				robots[req.r_num].st = state::cruiseToSender;
				robots[req.r_num].goal_msg.header.seq = robots[req.r_num].m;
				robots[req.r_num].m++;
				robots[req.r_num].goal_msg.header.stamp = ros::Time::now();
				robots[req.r_num].goal_msg.header.frame_id = "map";
			
				robots[req.r_num].goal_msg.pose.position.x = req.x;
				robots[req.r_num].goal_msg.pose.position.y = req.y;
				robots[req.r_num].goal_msg.pose.position.z = 0;
	
		
				robots[req.r_num].goal_msg.pose.orientation.x = 0;
				robots[req.r_num].goal_msg.pose.orientation.y = 0;
				robots[req.r_num].goal_msg.pose.orientation.z = 0;
				robots[req.r_num].goal_msg.pose.orientation.w = req.theta;
	
				if(req.r_num == 0){
					r0_old_position[0] = r0_actual_position[0];
					r0_old_position[1] = r0_actual_position[1];
					r0_addressee_position[0] = robots[req.r_num].goal_msg.pose.position.x;
					r0_addressee_position[1] = robots[req.r_num].goal_msg.pose.position.y;
				} else {
					r1_old_position[0] = r1_actual_position[0];
					r1_old_position[1] = r1_actual_position[1];
					r1_addressee_position[0] = robots[req.r_num].goal_msg.pose.position.x;
					r1_addressee_position[1] = robots[req.r_num].goal_msg.pose.position.y;
				}
				robots[req.r_num].publish = 1;
				robots[req.r_num].cruising = 1;
			}
		}
		else if(req.room_command == "delete") res.response = "invalid";
		else if(req.room_command == "quit"){
			roomCheckout(req.room_send);
			res.response = "quit";
		}
		else if(req.room_command == req.room_send) res.response = "call";
		
		else res.response = "call";
		return true;
	}


	if(req.room_send != robots[0].send && req.room_send != robots[1].send){
		if(req.room_command == "quit"){
			res.response = "quit";
			roomCheckout(req.room_send);
		}
		else if(req.r_num == 0){
			if(req.room_command == "call") res.response = "retry";
			else res.response = "invalid";
		}
		else res.response = "busy";
		return true;
	}


	if(robots[req.r_num].st == state::cruiseToSender){
		if(req.room_command == "call") res.response = "wait";
		else if(req.room_command == "delete"){
			returnToBase(req.r_num);
			robots[req.r_num].send = "";
			robots[req.r_num].st = state::available;
			res.response = "delete";
		}
		else if(req.room_command == "quit"){
			returnToBase(req.r_num);
			robots[req.r_num].send = "";
			robots[req.r_num].st = state::available;
			roomCheckout(req.room_send);
			res.response = "quit";
		}
		else res.response = "wait";
		return true;
	}


	if(robots[req.r_num].st == state::sender){
		if(req.room_command == "call") res.response = "invalid";
		else if(req.room_command == "delete"){
			returnToBase(req.r_num);
			robots[req.r_num].send = "";
			robots[req.r_num].st = state::available;
			res.response = "delete";
		}
		else if(req.room_command == "quit"){
			returnToBase(req.r_num);
			robots[req.r_num].send = "";
			robots[req.r_num].st = state::available;
			roomCheckout(req.room_send);
			res.response = "quit";
		}
		else{
			check2 = 0;
			find = 0;
			while(check2 < rooms.size()){
				if (rooms[check2] == req.room_command){
					find = 1;
					break;
				}
				check2++;
			}
			if(find){
				if(req.room_command == robots[req.r_num].send) res.response = "invalid";
				else if(req.room_command == robots[0].addr ||  req.room_command == robots[1].addr || req.room_command == robots[0].send || req.room_command == robots[1].send) res.response = "occupied";
				else{
					robots[req.r_num].addr = req.room_addr;
					res.response = "addressee";
					robots[req.r_num].st = state::cruiseToAddressee;
					robots[req.r_num].goal_msg.header.seq = robots[req.r_num].m;
					robots[req.r_num].m++;
					robots[req.r_num].goal_msg.header.stamp = ros::Time::now();
					robots[req.r_num].goal_msg.header.frame_id = "map";
				
					robots[req.r_num].goal_msg.pose.position.x = req.x;
					robots[req.r_num].goal_msg.pose.position.y = req.y;
					robots[req.r_num].goal_msg.pose.position.z = 0;

	
					robots[req.r_num].goal_msg.pose.orientation.x = 0;
					robots[req.r_num].goal_msg.pose.orientation.y = 0;
					robots[req.r_num].goal_msg.pose.orientation.z = 0;
					robots[req.r_num].goal_msg.pose.orientation.w = req.theta;

		
					if(req.r_num == 0){
						r0_old_position[0] = r0_actual_position[0];
						r0_old_position[1] = r0_actual_position[1];
						r0_addressee_position[0] = robots[req.r_num].goal_msg.pose.position.x;
						r0_addressee_position[1] = robots[req.r_num].goal_msg.pose.position.y;
					} else {
						r1_old_position[0] = r1_actual_position[0];
						r1_old_position[1] = r1_actual_position[1];
						r1_addressee_position[0] = robots[req.r_num].goal_msg.pose.position.x;
						r1_addressee_position[1] = robots[req.r_num].goal_msg.pose.position.y;
					}
					robots[req.r_num].publish = 1;
					robots[req.r_num].cruising = 1;
				}
			}
			else res.response = "inactive";
		}
		return true;
	}


	if(robots[req.r_num].st == state::cruiseToAddressee){
		if(req.room_command == "call") res.response = "invalid";
		else if(req.room_command == "delete"){
			/*robots[req.r_num].com.addressee = robots[req.r_num].addr;
			robots[req.r_num].com.sender = "delete";
			robots[req.r_num].com.r_num = req.r_num;
			robots[req.r_num].com.arrived = true;
			robots[req.r_num].com.taken = true;
			robots[req.r_num].com.failed = true;
			robots[req.r_num].clientsPublish = 1;*/
			robots[req.r_num].addr = "";
			robots[req.r_num].st = state::retToSender;
			res.response = "deleted_addressee";
			returnToSender(req.r_num);
		}
		else res.response = "invalid";
		return true;
	}


	if(robots[req.r_num].st == state::retToSender){
		res.response = "wait";
		return true;
	}


	if(robots[req.r_num].st == state::addressee){
		res.response = "wait";
		return true;
	}
}


void positionCallback(const tf2_msgs::TFMessage& tf){
	if(tfBuf.canTransform("map", "robot_0/base_link", ros::Time(0))!=0){
		geometry_msgs::TransformStamped stamp;
		stamp = tfBuf.lookupTransform("map", "robot_0/base_link", ros::Time(0));
		r0_actual_position[0] = stamp.transform.translation.x;
		r0_actual_position[1] = stamp.transform.translation.y;
	}
	if(tfBuf.canTransform("map", "robot_1/base_link", ros::Time(0))!=0){
		geometry_msgs::TransformStamped stamp;
		stamp = tfBuf.lookupTransform("map", "robot_1/base_link", ros::Time(0));
		r1_actual_position[0] = stamp.transform.translation.x;
		r1_actual_position[1] = stamp.transform.translation.y;
	}
}

void stuckCheckCallback(const ros::TimerEvent& event){
	float d_prec0, d_addr0, d_prec1, d_addr1, d_robots;
	d_prec0 = sqrt(pow(r0_actual_position[0] - r0_old_position[0],2) + pow(r0_actual_position[1] - r0_old_position[1],2));
	d_prec1 = sqrt(pow(r1_actual_position[0] - r1_old_position[0],2) + pow(r1_actual_position[1] - r1_old_position[1],2));
	d_addr0 = sqrt(pow(r0_actual_position[0] - r0_addressee_position[0],2) + pow(r0_actual_position[1] - r0_addressee_position[1],2));
	d_addr1 = sqrt(pow(r1_actual_position[0] - r1_addressee_position[0],2) + pow(r1_actual_position[1] - r1_addressee_position[1],2));
	r0_d_comp[0] = r0_d_comp[1];
	r0_d_comp[1] = d_prec0;
	r1_d_comp[0] = r1_d_comp[1];
	r1_d_comp[1] = d_prec1;
	for(int rob_num=0; rob_num<2; rob_num++){
		if(robots[rob_num].cruising){
			ROS_INFO("Stuck check");

			d_robots = sqrt(pow(r0_actual_position[0] - r1_actual_position[0],2) + pow(r0_actual_position[1] - r1_actual_position[1],2));
		
			if(d_robots < 1.5){
				ROS_INFO("Collision between robots");
				robots[0].res_msg=robots[0].goal_msg;
				robots[1].res_msg=robots[1].goal_msg;
				robots[0].goal_msg.pose.position.x=r0_actual_position[0];
				robots[0].goal_msg.pose.position.y=r0_actual_position[1];
				robots[1].goal_msg.pose.position.x=r1_actual_position[0];
				robots[1].goal_msg.pose.position.y=r1_actual_position[1];
				collisionResolution = 1;
				break;
			}
			if(d_prec0==0) robots[0].publish = 1;
			if(r0_d_comp[0]==r0_d_comp[1] && d_addr0>0.5 && d_prec0 && !collisionResolution && rob_num==0){
				std::cout << "Robot number "<< rob_num << " is stuck" << std::endl;
				std::this_thread::sleep_for(std::chrono::seconds(2));
				if(robots[rob_num].st == state::available) returnToBase(rob_num);
				if(robots[rob_num].st == state::cruiseToSender){
					//stuck0 = 1;
					robots[rob_num].publish = 1;
					robots[rob_num].com.addressee = "stuckInCalling";
					robots[rob_num].com.sender = robots[rob_num].send;
					robots[rob_num].com.r_num = rob_num;
					robots[rob_num].com.arrived = true;
					robots[rob_num].com.taken = true;
					robots[rob_num].com.failed = true;
					robots[rob_num].clientsPublish = 1;
					robots[rob_num].st = state::available;
					robots[rob_num].send = "";
					returnToBase(rob_num);
				}
				if(robots[rob_num].st == state::cruiseToAddressee && !collisionResolution){
					//stuck0 = 1;
					robots[rob_num].publish = 1;
					robots[rob_num].com.addressee = "stuckInSending";
					robots[rob_num].com.sender = robots[rob_num].send;
					robots[rob_num].com.r_num = rob_num;
					robots[rob_num].com.arrived = true;
					robots[rob_num].com.taken = true;
					robots[rob_num].com.failed = true;
					robots[rob_num].clientsPublish = 1;
					robots[rob_num].st = state::retToSender;
					robots[rob_num].addr = "";
					returnToSender(rob_num);
				}
				if(robots[rob_num].st == state::retToSender && !collisionResolution) returnToSender(rob_num);
			}
			
			if(d_prec1==0) robots[1].publish = 1;
			if(r1_d_comp[0]==r1_d_comp[1] && d_addr1>0.5 && d_prec1 && !collisionResolution && rob_num==1){
				std::cout << "Robot number "<< rob_num << " is stuck" << std::endl;
				//std::this_thread::sleep_for(std::chrono::seconds(2));
				if(robots[rob_num].st == state::available) returnToBase(rob_num);
				if(robots[rob_num].st == state::cruiseToSender){
					//stuck1 = 1;
					robots[rob_num].publish = 1;
					robots[rob_num].com.addressee = "stuckInCalling";
					robots[rob_num].com.sender = robots[rob_num].send;
					robots[rob_num].com.r_num = rob_num;
					robots[rob_num].com.arrived = true;
					robots[rob_num].com.taken = true;
					robots[rob_num].com.failed = true;
					robots[rob_num].clientsPublish = 1;
					robots[rob_num].st = state::available;
					robots[rob_num].send = "";
					returnToBase(rob_num);
				}
				if(robots[rob_num].st == state::cruiseToAddressee && !collisionResolution){
					//stuck1 = 1;
					robots[rob_num].publish = 1;
					robots[rob_num].com.addressee = "stuckInSending";
					robots[rob_num].com.sender = robots[rob_num].send;
					robots[rob_num].com.r_num = rob_num;
					robots[rob_num].com.arrived = true;
					robots[rob_num].com.taken = true;
					robots[rob_num].com.failed = true;
					robots[rob_num].clientsPublish = 1;
					robots[rob_num].st = state::retToSender;
					robots[rob_num].addr = "";
					returnToSender(rob_num);
				}
				if(robots[rob_num].st == state::retToSender && !collisionResolution) returnToSender(rob_num);
			}
		
			if(d_addr0<0.5 && rob_num==0){
				std::cout << "Robot number "<< rob_num << " is arrived at the addressee" << std::endl;
				robots[rob_num].cruising = 0;
				if(robots[rob_num].st == state::cruiseToSender || robots[rob_num].st == state::retToSender){
					robots[rob_num].st = state::sender;
					robots[rob_num].com.addressee = "arrivedAtSender";
					robots[rob_num].com.sender = robots[rob_num].send;
					robots[rob_num].com.r_num = rob_num;
					robots[rob_num].com.arrived = true;
					robots[rob_num].com.taken = true;
					robots[rob_num].com.failed = true;
					robots[rob_num].clientsPublish = 1;
				}
				if(robots[rob_num].st == state::cruiseToAddressee){
					robots[rob_num].com.addressee = robots[rob_num].addr;
					robots[rob_num].com.sender = robots[rob_num].send;
					robots[rob_num].com.r_num = rob_num;
					robots[rob_num].com.arrived = true;
					robots[rob_num].com.taken = false;
					robots[rob_num].com.failed = false;
					robots[rob_num].clientsPublish = 1;				
				}
			}
			if(d_addr1<0.5 && rob_num==1){
				std::cout << "Robot number "<< rob_num << " is arrived at the addressee" << std::endl;
				robots[rob_num].cruising = 0;
				if(robots[rob_num].st == state::cruiseToSender || robots[rob_num].st == state::retToSender){
					robots[rob_num].st = state::sender;
					robots[rob_num].com.addressee = "arrivedAtSender";
					robots[rob_num].com.sender = robots[rob_num].send;
					robots[rob_num].com.r_num = rob_num;
					robots[rob_num].com.arrived = true;
					robots[rob_num].com.taken = true;
					robots[rob_num].com.failed = true;
					robots[rob_num].clientsPublish = 1;
				}
				if(robots[rob_num].st == state::cruiseToAddressee){
					robots[rob_num].com.addressee = robots[rob_num].addr;
					robots[rob_num].com.sender = robots[rob_num].send;
					robots[rob_num].com.r_num = rob_num;
					robots[rob_num].com.arrived = true;
					robots[rob_num].com.taken = false;
					robots[rob_num].com.failed = false;
					robots[rob_num].clientsPublish = 1;				
				}
			}
		}
	}
}

void clientsCallback(const thesis_pad::Comunication msg){
	if(msg.r_num==0){
		if(msg.taken && msg.arrived && !msg.failed){
			robots[0].st = state::available;
			robots[0].addr = "";
			robots[0].send = "";
			returnToBase(0);
		}
		if(msg.failed && !msg.taken){
			robots[0].st = state::retToSender;
			robots[0].addr = "";
			returnToSender(0);
		}
	} else {
		if(msg.taken && msg.arrived && !msg.failed){
			robots[1].st = state::available;
			robots[1].addr = "";
			robots[1].send = "";
			returnToBase(1);
		}
		if(msg.failed && !msg.taken){
			robots[1].st = state::retToSender;
			robots[1].addr = "";
			returnToSender(1);
		}
	}
}
		


int main(int argc, char **argv){
	std::vector<geometry_msgs::Twist> resolution;

	geometry_msgs::Twist reverse;
	geometry_msgs::Twist turn_one;
	geometry_msgs::Twist turn_two;
	geometry_msgs::Twist turn_three;
	geometry_msgs::Twist turn_four;
	geometry_msgs::Twist turn_five;
	geometry_msgs::Twist restart;
	reverse.linear.x = -15.0;
	reverse.linear.y = -15.0;
	reverse.angular.z = 2.0;
	turn_two.angular.z = 2.0;
	turn_three.angular.z = 2.0;
	turn_four.angular.z = 2.0;
	turn_five.angular.z = 2.0;
	restart.linear.x = 10.0;
	resolution.push_back(reverse);
	resolution.push_back(turn_one);
	resolution.push_back(reverse);
	resolution.push_back(turn_two);
	resolution.push_back(turn_three);
	resolution.push_back(turn_four);
	resolution.push_back(turn_five);


	robot_0.st = state::available;
	robot_0.name = "Robot 0";
	robot_0.cruising = 0;
	robot_0.publish = 0;
	robot_0.m = 0;
	r0_base_position[0] = 51.5;
	r0_base_position[1] = 18.7;
	robot_0.clientsPublish = 0;

	robot_1.st = state::available;
	robot_1.name = "Robot 1";
	robot_1.cruising = 0;
	robot_1.publish = 0;
	robot_1.m = 0;
	r1_base_position[0] = 51.4;
	r1_base_position[1] = 6.5;
	robot_1.clientsPublish = 0;

	robots.push_back(robot_0);
	robots.push_back(robot_1);

	ros::init(argc, argv, "Server_PickAndDelivery");
	ROS_INFO("Hello, the server is operating");
	ros::NodeHandle n;
	ros::Subscriber room_sub = n.subscribe("server", 1000, roomCallback);
	ros::ServiceServer set_adr = n.advertiseService("addressee", setAddresseeService);
	ros::Publisher goal_pub_r0 = n.advertise<geometry_msgs::PoseStamped>("/robot_0/move_base_simple/goal", 1000);
	ros::Publisher goal_pub_r1 = n.advertise<geometry_msgs::PoseStamped>("/robot_1/move_base_simple/goal", 1000);
	ros::Publisher cmd_pub_r0 = n.advertise<geometry_msgs::Twist>("/robot_0/cmd_vel", 1000);
	ros::Publisher cmd_pub_r1 = n.advertise<geometry_msgs::Twist>("/robot_1/cmd_vel", 1000);
	ros::Publisher clients_pub = n.advertise<thesis_pad::Comunication>("clients", 1000);
	ros::Subscriber clients_sub = n.subscribe("clients", 1000, clientsCallback);


	tf2_ros::TransformListener tfList(tfBuf);

	ros::Rate loop_rate(100);
	
	ros::Subscriber sub_tf = n.subscribe("tf", 1000, positionCallback);

	ros::Timer stuck_t = n.createTimer(ros::Duration(0.5), stuckCheckCallback);

	int count=0;
	int time0=0;
	int time1=0;
	while(ros::ok()){
		if(stuck0){
			stuck0 = 0;
			if(time0==1){
				cmd_pub_r0.publish(resolution[0]);
				time0++;
			}
			if(time0==2){
				cmd_pub_r0.publish(resolution[3]);
				time0=0;
			}
			if(time0==0) time0++;
		}
		if(stuck1){
			stuck1 = 0;
			if(time1==1){
				cmd_pub_r1.publish(resolution[0]);
				time1++;
			}
			if(time1==2){
				cmd_pub_r1.publish(resolution[3]);
				time1=0;
			}
			if(time1==0) time1++;
		}
		if(collisionResolution){
			ROS_INFO("So qui");
			collisionResolution = 0;
			/*cmd_pub_r0.publish(resolution[0]);
			cmd_pub_r1.publish(resolution[0]);
			std::this_thread::sleep_for(std::chrono::seconds(1));
			robots[0].res_msg=robots[0].goal_msg;
			robots[1].res_msg=robots[1].goal_msg;
			robots[0].goal_msg.pose.position.x=r0_actual_position[0];
			robots[0].goal_msg.pose.position.y=r0_actual_position[1];
			robots[1].goal_msg.pose.position.x=r1_actual_position[0];
			robots[1].goal_msg.pose.position.y=r1_actual_position[1];
			goal_pub_r0.publish(robots[0].goal_msg);
			goal_pub_r1.publish(robots[1].goal_msg);*/
			for(int step=0; step<4; step++){
				std::this_thread::sleep_for(std::chrono::seconds(1));
				std::cout<<"Step n "<<step<< std::endl;
				if(robots[0].st != state::sender && robots[0].st != state::addressee) cmd_pub_r0.publish(resolution[0]);
				if(robots[1].st != state::sender && robots[1].st != state::addressee) cmd_pub_r1.publish(resolution[0]);
			}
		}
		for(int rob_num=0; rob_num<2; rob_num++){
			if(robots[rob_num].clientsPublish){
				robots[rob_num].clientsPublish = 0;
				clients_pub.publish(robots[rob_num].com);
			}
			if(robots[rob_num].publish){
				std::cout << "Robot number "<< rob_num <<" has received a new goal"<< std::endl;
				if(rob_num == 0) goal_pub_r0.publish(robots[rob_num].goal_msg);
				else goal_pub_r1.publish(robots[rob_num].goal_msg);
				robots[rob_num].publish = 0;
			}
		}
		ros::spinOnce();
		loop_rate.sleep();
		count++;
	}
}
